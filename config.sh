#!/bin/sh

set -e

CDIR=$(postconf -h config_directory)
echo "Config directory: ${CDIR}"
sed -e 's|@@CDIR@@|'"${CDIR}"'|g' postfix-policyd-spf-perl.in > postfix-policyd-spf-perl
